module Mosaic
  class GridFactory
    def initialize(data)
      @data = data
      @grid = Grid.new
    end

    def setup_grid
      @data.each_with_index do |cells_data, i|
        cells_data.each_with_index do |cell_data, j|
          @grid.add_cell Cell.new(@grid, cell_data, Position.new(i, j))
        end
      end

      @grid
    end
  end
end
