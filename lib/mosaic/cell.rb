module Mosaic
  class Cell
    attr_reader :neighbour_count, :position
    attr_accessor :state

    ROLE_DICTIONARY = {
      /0/ => Role::Killer,
      /[1-9]/ => Role::Creator,
      /\s*/ => Role::Passive
    }

    def initialize(grid, neighbour_count, position)
      @grid = grid
      @neighbour_count = neighbour_count.to_s.slice(/\d+/)
      @position = position
      @state = States::Null.instance
      extend(role_by_neighbour_count)
    end

    def self_with_neighbours
      @grid.cells.select do |cell|
        (position.x.pred..position.x.succ) === cell.position.x &&
        (position.y.pred..position.y.succ) === cell.position.y
      end
    end

    def mark_null_self_and_neighbours
      self_with_neighbours.select(&null_state_cells).first(revivable_neighbour_count).each do |cell|
        cell.state = applicable_state
      end
    end

    private ###

      def revivable_neighbour_count
        count = effective_neighbour_count - alive_neighbour_count
        count > 0 ? count : 0
      end

      def alive_neighbour_count
        self_with_neighbours.select(&alive_state_cells).size
      end

      def null_state_cells
        Proc.new { |cell| cell.state.is_null? }
      end

      def alive_state_cells
        Proc.new { |cell| cell.state.alive? }
      end

      def role_by_neighbour_count
        ROLE_DICTIONARY.find { |k, v| neighbour_count.to_s.match(k) }.pop
      end

    ### private
  end
end
