module Mosaic
  module Role
    module Killer
      def effective_neighbour_count
        9
      end

      def applicable_state
        States::Dead.instance
      end
    end
  end
end
