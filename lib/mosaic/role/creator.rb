module Mosaic
  module Role
    module Creator
      def effective_neighbour_count
        neighbour_count.to_i
      end

      def applicable_state
        States::Alive.instance
      end
    end
  end
end
