module Mosaic
  module Role
    module Passive
      def effective_neighbour_count
        0
      end

      def applicable_state
        States::Null.instance
      end
    end
  end
end
