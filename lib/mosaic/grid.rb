module Mosaic
  class Grid
    def initialize
      @cells = []
    end

    def cells
      @cells.dup
    end

    def add_cell(cell)
      @cells << cell
    end
  end
end
