require 'singleton'

module Mosaic
  module States
    class Base
      include Singleton

      def alive?
        false
      end

      def output
        ' '
      end

      def is_null?
        false
      end
    end
  end
end
