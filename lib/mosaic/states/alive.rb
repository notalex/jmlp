module Mosaic
  module States
    class Alive < Base
      def alive?
        true
      end

      def output
        '#'
      end
    end
  end
end
