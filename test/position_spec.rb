require 'support/test_helper'

describe "Position" do
  it "must create a valid object" do
    position = Position.new(0, 1)
    position.x.must_equal 0
    position.y.must_equal 1
  end
end
