require 'support/test_helper'

describe "Cell" do
  it "must create a valid cell object" do
    position = Position.new(1, 0)
    cell = Cell.new(Grid.new, 5, position)
    cell.effective_neighbour_count.must_equal 5
    cell.position.x.must_equal 1
  end

  it "must have a valid killer role" do
    position = Position.new(1, 0)
    cell = Cell.new(Grid.new, 0, position)
    cell.must_be_kind_of Role::Killer
  end

  it "must have a valid creator role" do
    position = Position.new(1, 0)
    cell = Cell.new(Grid.new, 9, position)
    cell.must_be_kind_of Role::Creator
  end

  it "must have a valid passive role" do
    position = Position.new(1, 0)
    cell = Cell.new(Grid.new, ' ', position)
    cell.must_be_kind_of Role::Passive
  end

  it "must have a valid passive role" do
    position = Position.new(1, 0)
    cell = Cell.new(Grid.new, ' ', position)
    cell.must_be_kind_of Role::Passive
  end

  it "must reveal its neighbours" do
    data = [['4', ' '], [' ', ' ']]
    grid = GridFactory.new(data).setup_grid
    grid.cells[0].self_with_neighbours.size.must_equal 4
  end

  it "must have valid neighbour state" do
    data = [['4', ' '], [' ', ' ']]
    grid = GridFactory.new(data).setup_grid
    grid.cells[0].applicable_state.must_be_instance_of States::Alive
    grid.cells[1].applicable_state.must_be_instance_of States::Null
  end

  it "must be able to mark neighbours" do
    data = [['4', ' '], [' ', ' ']]
    grid = GridFactory.new(data).setup_grid
    grid.cells[0].mark_null_self_and_neighbours
    grid.cells.map { |cell| cell.state.alive? }.must_equal [true, true, true, true]
  end
end
