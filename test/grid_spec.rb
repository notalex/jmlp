require 'support/test_helper'

describe "GridFactory" do
  it "must create cells from 2 dimensional array" do
    data = [['0', ' '], [' ', '9']]
    grid = GridFactory.new(data).setup_grid
    grid.cells[1].position.y.must_equal 1
    grid.cells[3].position.x.must_equal 1
  end

  it "must apply states correctly based on input" do
    data = [[' ', '1'], [' ', ' ']]
    grid = GridFactory.new(data).setup_grid
    grid.cells.each &:mark_null_self_and_neighbours
    grid.cells.map { |cell| cell.state.output  }.must_equal ['#', ' ', ' ', ' ']
  end

  it "must handle conflicting creator cells" do
    data = [[' ', '2'], ['2', ' ']]
    grid = GridFactory.new(data).setup_grid
    grid.cells.each &:mark_null_self_and_neighbours
    grid.cells.map { |cell| cell.state.output  }.must_equal ['#', '#', ' ', ' ']
  end
end
