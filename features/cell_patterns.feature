Feature: Cell pattern

  Scenario: All alive pattern
    Given a file named "start_grid" with:
    """
    |4|3|
    |2| |
    """
    When I run mosaic start_grid
    Then the output should contain exactly:
    """
    |#|#|
    |#|#|
    """

  Scenario: Pattern with two conflicting creator cells
    Given a file named "start_grid" with:
    """
    | |2|
    |2| |
    """
    When I run mosaic start_grid
    Then the output should contain exactly:
    """
    |#|#|
    | | |
    """
