Given(/^a file named "(.+)" with:$/) do |file_name, content|
  path = File.join('/tmp', file_name)
  File.open(path, 'w') { |f| f.write(content) }
end

When(/^I run mosaic (.+)$/) do |file_name|
  @output = %x[bin/mosaic #{ file_name }]
end

Then(/^the output should contain exactly:$/) do |content|
  parsed_content = content.gsub(/\n\|/, '').gsub(/^\||\|$/, '').split('|')
  eval(@output).must_equal parsed_content
end
